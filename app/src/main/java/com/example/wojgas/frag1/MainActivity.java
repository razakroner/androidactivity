package com.example.wojgas.frag1;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction fg = getSupportFragmentManager().beginTransaction();

        int displayMode = getResources().getConfiguration().orientation;

        switch (displayMode) {
            case Configuration.ORIENTATION_PORTRAIT:
                Fragment f1 = new Frag1();
                fg.replace(android.R.id.content, f1).commit();
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                Fragment f2 = new Frag2();
                fg.replace(android.R.id.content, f2).commit();
                break;
        }
    }
}
