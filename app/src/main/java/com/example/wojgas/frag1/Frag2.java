package com.example.wojgas.frag1;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by vojgas on 27.11.2017.
 */

public class Frag2 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup vg,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag2, vg, false);
    }

    public void setText(String text) {
        TextView view = getView().findViewById(R.id.frag2Text);
        view.setText(text);
    }
}
